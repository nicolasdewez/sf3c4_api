<?php

namespace AppBundle\Controller;

use AppBundle\Entity\City;
use AppBundle\Form\CityType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/api/cities")
 */
class CityApiController extends FOSRestController
{
    /**
     * @ApiDoc(
     *     section="City",
     *     description="Get all cities",
     *     output="AppBundle\Entity\City[]",
     *     statusCodes={
     *         200 = "Returned when successful"
     *     }
     * )
     *
     * @Rest\Route("", name="app_api_cities", methods={"GET"})
     * @Rest\View
     */
    public function listAction(): array
    {
        return $this->get('doctrine')->getRepository(City::class)->findAll();
    }

    /**
     * @ApiDoc(
     *     section="City",
     *     description="Get a city",
     *     output="AppBundle\Entity\City",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Returned if city not found"
     *     }
     * )
     *
     * @Rest\Route("/{id}", name="app_api_city", methods={"GET"})
     * @Rest\View
     */
    public function getAction(City $city): City
    {
        return $city;
    }

    /**
     * @ApiDoc(
     *     section="City",
     *     description="Create a city",
     *     input="CityType::class",
     *     statusCodes={
     *         201 = "Returned when successful",
     *         400 = "Returned when errors found"
     *     },
     *     responseMap={
     *         201 = {"class" = "AppBundle\Entity\City"},
     *         400 = {"class" = "Symfony\Component\Form\FormErrorIterator"}
     *     }
     * )
     *
     * @Rest\Route("", name="app_api_create_city", methods={"POST"})
     * @Rest\View
     */
    public function createAction(Request $request)
    {
        $city = new City();
        $form = $this->createForm(CityType::class, $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('doctrine')->getEntityManager()->persist($city);
            $this->get('doctrine')->getEntityManager()->flush();

            return $city;
        }

        return $form->getErrors(true);
    }

    /**
     * @Rest\Route("/{id}", name="app_api_edit_city", methods={"PUT"})
     * @Rest\View
     */
    public function editAction(City $city, Request $request)
    {
        $form = $this->createForm(CityType::class, $city);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('doctrine')->getEntityManager()->flush();

            return $city;
        }

        return $form->getErrors(true);
    }
}
